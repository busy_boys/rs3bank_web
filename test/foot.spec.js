import { mount } from "@vue/test-utils";
import foot from "~/components/foot.vue";

describe('foot', () => {
  test('is a Vue instance with footer', () => {
  const wrapper = mount(foot);
  expect(wrapper.isVueInstance()).toBeTruthy()
  })
});
