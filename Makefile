gp:
	git pull
docker_build:
	docker build --rm -t rs3_bank .
docker_rm:
	docker rm -f rs3bank || true
docker_run:
	docker run --restart=always -d -l traefik.frontend.rule=Host:rstreebank.ru --name rs3bank rs3_bank

docker_rm_dev:
	docker rm -f dev || true
docker_run_dev:
	docker run --restart=always -d --name dev rs3_bank


docker: docker_build docker_rm docker_run
	docker restart rs3bank
docker_dev: docker_build docker_rm_dev docker_run_dev
	docker restart dev

deploy: gp docker
deploy_dev: gp docker_dev
