export API_BASE = 'https://api.rstreebank.ru/'
export INSTRUCTION_LINK = 'https://docs.google.com/document/d/1wd-sgGyIo5AQq2IPj6jWa_QmU0fUohXj48qsfVDgcBs/edit'
export RST_PUBLICATION = API_BASE + 'doc/RST\ towards\ a\ functional\ theory\ of\ text\ organization.pdf'

export default {
  API_BASE
  INSTRUCTION_LINK
  RST_PUBLICATION
}

###
module.exports = {
  API_BASE
}
###
