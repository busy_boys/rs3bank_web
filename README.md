# [rs3bank](https://rstreebank.ru/)

> new website of RS3 BANK

[Production](https://rstreebank.ru): [![pipeline status](https://gitlab.rexhaif.xyz/best_boys/rs3bank/badges/production/pipeline.svg?style=flat-square)](https://gitlab.rexhaif.xyz/best_boys/rs3bank/commits/production)
[![coverage report](https://gitlab.rexhaif.xyz/best_boys/rs3bank/badges/production/coverage.svg?style=flat-square)](https://gitlab.rexhaif.xyz/best_boys/rs3bank/commits/production)

[Development](https://dev.rstreebank.ru): [![pipeline status](https://gitlab.rexhaif.xyz/best_boys/rs3bank/badges/master/pipeline.svg?style=flat-square)](https://gitlab.rexhaif.xyz/best_boys/rs3bank/commits/master)
[![coverage report](https://gitlab.rexhaif.xyz/best_boys/rs3bank/badges/master/coverage.svg?style=flat-square)](https://gitlab.rexhaif.xyz/best_boys/rs3bank/commits/master)

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start
```

## Usefull links

- [Vue](https://vuejs.org/)
- [Nuxt](https://nuxtjs.org/)
- [Vue-bootstrap](https://bootstrap-vue.js.org/)
- [dev version of site](https://dev.rstreebank.ru)
  <!-- - [sites traefik](https://traefik.rstreebank.ru/dashboard/) -->
