"use strict";

const Koa = require("koa");
const KoaRouter = require("koa-router");
const Body = require("koa-body");
const cors = require("@koa/cors");
const {
  ready
} = require("consola");
const {
  Nuxt,
  Builder
} = require("nuxt");
const nodemailer = require("nodemailer");

const config = require("../nuxt.config.js");

const app = new Koa();
const router = new KoaRouter();
app.use(Body()).use(cors());

const nuxt = new Nuxt(config);

const {
  host = process.env.HOST || "127.0.0.1",
  port = process.env.PORT || 3000,
} = nuxt.options.server;
const dev = app.env !== "production";

const transporter = nodemailer.createTransport({
  host: "smtp.yandex.com",
  secure: true,
  port: 465,
  auth: {
    user: process.env.USER,
    pass: process.env.PASS
  }
});

async function start() {
  if (dev) {
    const builder = new Builder(nuxt);
    await builder.build();
  } else {
    await nuxt.ready();
  }

  router
    .post(
      "/contact",
      Body({
        multipart: true
      }),
      handle_form
    )
    .post(
      "/rus/contact",
      Body({
        multipart: true
      }),
      handle_form
    )
    .post(
      "/eng/contact",
      Body({
        multipart: true
      }),
      handle_form
    )
    .get("(.*)", render);

  app.use(router.routes()).use(router.allowedMethods());

  async function render(ctx) {
    ctx.status = 200;
    ctx.respond = false; // Bypass Koa's built-in response handling
    ctx.req.ctx = ctx; // This might be useful later on, e.g. in nuxtServerInit or with nuxt-stash
    nuxt.render(ctx.req, ctx.res);
  }

  async function handle_form(ctx) {
    var today = new Date();

    ctx.status = 200;
    ctx.respond = false;
    const form = ctx.request.body;
    form.date = today;

    console.log(JSON.stringify(form));

    let MailOptions = {
      from: `"${form.name}" <from@rstreebank.ru>`,
      to: "dinabpr@gmail.com, marya.kobozeva@gmail.com",
      bcc: "debug@rstreebank.ru",
      subject: `Feedback about ${form.subject} by ${form.email}`,
      text: form.msg
    };
    let DevMailOptions = {
      from: `"${form.name}" <from@rstreebank.ru>`,
      to: "debug@rstreebank.ru",
      subject: `Feedback about ${form.subject} by ${form.email}`,
      text: form.msg
    };
    transporter.sendMail(dev ? DevMailOptions : MailOptions, (error, info) => {
      if (error) {
        return console.log(error);
      }
      console.log("Message sent: %s", info.messageId);
    });
  }

  app.listen(port, host);

  ready(`Server listening on https://${host}:${port}`);
}
start();
